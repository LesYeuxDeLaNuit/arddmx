#ifndef DmxSerial_h
#define DmxSerial_h

#include <avr/io.h>

// ----- Constants -----

#define DMXSERIAL_MAX 512 // max. number of supported DMX data channels

#define DMXMODEPIN D2     // Arduino pin 2 for controlling the data direction is the default value.
#define DmxModeOut HIGH  // set the level to HIGH for outgoing data direction
#define DmxModeIn  LOW   // set the level to LOW  for incomming data direction

#define DMXPROBE_RECV_MAX 50 // standard maximum of waiting for a DMX packet in DMXPROBE mode.

// ----- Enumerations -----

typedef enum { 
  DMXNone, // unspecified
  DMXController , // always sending
  DMXReceiver,   // always listening
  DMXProbe       // send and receive upon request
} DMXMode;

// ----- Library Class -----

extern "C" {
  typedef void (*dmxUpdateFunction)(void);
}

class DMXSerialClass
{
  public:
    void    init (int mode);
    void    init (int mode, int modePin);
    void    maxChannel (int channel);
    uint8_t read       (int channel);
    void    write      (int channel, uint8_t value);
    uint8_t *getBuffer();
    unsigned long noDataSince();
    bool dataUpdated();
    void resetUpdated();
    bool receive();
    bool receive(uint8_t wait);
    void    term();
    
  private:
};

extern DMXSerialClass DMXSerial;

#endif
