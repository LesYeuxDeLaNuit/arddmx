#include <DMXSerial.h>             // Importation de la Librairie DMXSerial  
#include "ws2812.h"                // Importation de WS2812 basé sur neopixel.h

#define NUM_LEDS 30                // Nombre de LED sur le bandau 
#define DMXSTART 1                 // Addresse du systeme
#define DMXLENGTH (NUM_LEDS*3)     // Nombre de channels DMX utilisé (3*30 LEDs)

void setup () {

  DMXSerial.init(DMXProbe);        // initialisation du bus DMX en mode manuel
  DMXSerial.maxChannel(DMXLENGTH); // initialisation de la "taille" du bandeau

  setupNeopixel();                 // setup des LED (Pin def dans in ws2812.h)
                                   //(arduino uno = Pin 12))

}


void loop() {
  
  if (DMXSerial.receive()) {      //Si on recoit un signale DMX 
    updateNeopixel(DMXSerial.getBuffer() + DMXSTART, NUM_LEDS); 
                                  //Faire coinsider les valeurs DMX
                                  //du debut du bandeausur et sur toute ca longeur  
  }

}
