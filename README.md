# ardDMX
---
## Matériel 
  * Arduino UNO
  * Bandeau LED WS2812b
  * MAX485 sur carte
  * Prises DMX mâle et femelle
  * Convertiseur AC/DC 5V
  * Câbles
---

## Montage du système
  * Câbler selon le schéma :

  ![Cf Schéma/DMX Sketch.png](https://gitlab.com/LesYeuxDeLaNuit/arddmx/-/raw/master/Schéma/DMX%20Sketch.png)
---

## Programmation
  * Télécharger et installer l'[IDE d'Arduino](https://www.arduino.cc/en/Main/Software)
  * Clone le code du dossier [Code](https://gitlab.com/LesYeuxDeLaNuit/arddmx/-/tree/master/Code)
  * Connecter la carte au PC
  * Ouvrir DMX.ino
  * **Ne pas oublier de mettre le COM et la carte dans les parametres**
  * Téléverser le code sur la carte
---

## The End